#!/bin/sh
set -e

mkdir -p tmp-deploy
cp deploy/install tmp-deploy/
cp deploy/wapi.service tmp-deploy/
cp deploy/nginx.conf tmp-deploy/
cp wapi tmp-deploy/wapi
cd tmp-deploy
tar --zstd -cf wapi.tar.zst \
     install wapi wapi.service nginx.conf

# Use a checksum file to signal that the deploy is complete.
sha256sum "wapi.tar.zst" > "wapi.tar.zst.sha256sum"

sftp -P "${SSH_DEPLOY_PORT}" -o "StrictHostKeyChecking=no" \
     "${SSH_DEPLOY_USER}"@wapi.alienscience.org:inbox/ << EOF
put -f *.tar.zst
put -f *.sha256sum
ls -lh
EOF
