module gitlab.com/alienscience/wapi

go 1.16

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/render v1.0.1
	github.com/nathan-osman/go-sunrise v1.0.0
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d
)
