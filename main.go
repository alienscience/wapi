package main

import (
	"flag"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"sort"
	"sync/atomic"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

const (
	zeroKelvin          = 273.15
	dwdLatest           = "https://opendata.dwd.de/weather/local_forecasts/mos/MOSMIX_S/all_stations/kml/MOSMIX_S_LATEST_240.kmz"
	downloaded          = "MOSMIX_S_LATEST_240.kmz"
	minimumDownloadSize = 1 * 1024 * 1024 // 1MB
	maximumDownloadAge  = 10 * time.Minute
	listen              = "127.0.0.1:8090"
	sunFormat           = "15:04 MST"
	zone                = "Europe/Berlin"
	dwdTimeFormat       = "2006-01-02T15:04:05.999Z07"
)

var devMode = flag.Bool("dev", false, "run in dev mode")

type APIResult struct {
	Issuer      string
	IssueTime   string
	Description string
	Latitude    float32
	Longitude   float32
	Sunrise     string
	Sunset      string
	Forecast    []APIForecast
}

type APIForecast struct {
	Time           time.Time
	Temp           float32 // K
	Pressure       float32 // Pa at ground level
	Humidity       int     // %
	Clouds         int     // % Cover
	WindSpeed      float32 // m/s
	WindDeg        int     // degrees
	Rain           float32 // Rain last hour kg/m2
	RainProb       float32 // Rain probability
	WeatherWarning int     // Unit unknown
	Visibility     int     // m
	FogProb        float32 // Fog probability
	Snow           float32 // Snow Rain Equiv kg/m2
	Description    string
}

var neustadt atomic.Value

func updateForecast() {
	decoder, err := newDWDDecoder()
	if err != nil {
		log.Fatal(err)
	}
	ticker := time.NewTicker(60 * time.Minute)
	defer ticker.Stop()
	for {
		log.Print("Downloading forecast")
		err = downloadForecast()
		if err != nil {
			log.Print(err)
			<-ticker.C
			continue
		}
		log.Print("Decoding forecast")
		decodeCh := make(chan interface{})
		go decoder.run(downloaded, decodeCh)
		var definition ProductDefinition
		for ev := range decodeCh {
			switch el := ev.(type) {
			case ProductDefinition:
				definition = el
			case EnrichedPlacemark:
				if el.Name == "N3432" {
					res := apiResult(definition, el)
					neustadt.Store(res)
				}
			}
		}
		log.Print("Decoded forecast")
		<-ticker.C
	}
}

func downloadForecast() error {
	// Use an existing download file if the download is recent or if
	// in development mode
	existing, err := os.Stat(downloaded)
	if err == nil && existing.Size() > minimumDownloadSize {
		if *devMode {
			return nil
		}
		diff := time.Since(existing.ModTime())
		if diff < maximumDownloadAge {
			return nil
		}
	}
	// Download the DWD forecast to a file
	resp, err := http.Get(dwdLatest)
	if err != nil {
		return err
	}
	defer func() { _ = resp.Body.Close() }()
	out, err := os.Create(downloaded)
	if err != nil {
		return err
	}
	defer func() { _ = out.Close() }()
	_, err = io.Copy(out, resp.Body)
	return err
}

// Convert DWD datastructure to a result returned by the API
func apiResult(definition ProductDefinition, pm EnrichedPlacemark) APIResult {
	location, err := time.LoadLocation(zone)
	if err != nil {
		log.Fatal(err)
	}
	sunrise, sunset := sun(float64(pm.Latitude), float64(pm.Longitude), time.Now())
	ret := APIResult{
		Issuer:      definition.Issuer,
		IssueTime:   definition.IssueTime,
		Description: pm.Description,
		Latitude:    pm.Latitude,
		Longitude:   pm.Longitude,
		Forecast:    make([]APIForecast, len(definition.TimeSteps)),
		Sunrise:     sunrise.In(location).Format(sunFormat),
		Sunset:      sunset.In(location).Format(sunFormat),
	}
	toHourly(pm.Forecast["R602"].Values)
	for i, ts := range definition.TimeSteps {
		forecast := &ret.Forecast[i]
		t, err := time.Parse(dwdTimeFormat, ts)
		if err != nil {
			log.Fatal(err)
		}
		forecast.Time = t.In(location)
		temperature := pm.Forecast["TTT"].Values[i]
		forecast.Temp = temperature
		forecast.Pressure = pm.Forecast["PPPP"].Values[i]
		dewPoint := pm.Forecast["Td"].Values[i]
		forecast.Humidity = humidity(dewPoint, temperature)
		forecast.Clouds = int(pm.Forecast["N"].Values[i])
		forecast.WindSpeed = pm.Forecast["FF"].Values[i]
		forecast.WindDeg = int(pm.Forecast["DD"].Values[i])
		forecast.Rain = pm.Forecast["RR1c"].Values[i]
		forecast.RainProb = pm.Forecast["R602"].Values[i]
		forecast.WeatherWarning = int(pm.Forecast["ww"].Values[i])
		forecast.Visibility = int(pm.Forecast["VV"].Values[i])
		forecast.FogProb = pm.Forecast["wwM"].Values[i]
		forecast.Snow = pm.Forecast["RRS1c"].Values[i]
		forecast.Description = describe(forecast, dewPoint)
	}
	return ret
}

// Convert dew point to relative humidity in percent
func humidity(dewPoint float32, temp float32) int {
	// RH: =100*(EXP((17.625*TD)/(243.04+TD))/EXP((17.625*T)/(243.04+T)))
	td := dewPoint - zeroKelvin
	t := temp - zeroKelvin
	numerator := math.Exp(float64((17.625 * td) / (243.04 + td)))
	denominator := math.Exp(float64((17.625 * t) / (243.04 + t)))
	rh := 100.0 * numerator / denominator
	return int(rh)
}

// Interpolate a non-hourly forecast to an hourly forecast
func toHourly(forecast map[int]float32) {
	keys := make([]int, 0, len(forecast))
	for k := range forecast {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	li := 0
	lv := float32(0)
	for _, k := range keys {
		v := forecast[k]
		diff := k - li
		if diff == 0 {
			lv = v
			continue
		}
		step := (v - lv) / float32(diff)
		for i := 1; i < diff; i++ {
			guess := lv + float32(i)*step
			forecast[li+i] = float32(math.Round(float64(guess)))
		}
		li = k
		lv = v
	}
}

func (APIResult) Render(w http.ResponseWriter, r *http.Request) error {
	// No preprocessing needed when rendering an APIResult
	return nil
}

func threeDayForecast(w http.ResponseWriter, r *http.Request) {
	atom := neustadt.Load()
	if atom == nil {
		http.Error(w, "Waiting for weather data", http.StatusServiceUnavailable)
		return
	}
	placemark := atom.(APIResult)
	err := render.Render(w, r, placemark)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	go updateForecast()
	r := chi.NewRouter()
	r.Use(middleware.Recoverer)
	r.Route("/v1", func(r chi.Router) {
		r.Use(render.SetContentType(render.ContentTypeJSON))
		r.Get("/", threeDayForecast)
	})
	log.Fatal(http.ListenAndServe(listen, r))
}
