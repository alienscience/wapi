package main

import (
	"archive/zip"
	"embed"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"

	"golang.org/x/net/html/charset"
)

const (
	definitions = "MetElementDefinition.xml"
)

//go:embed MetElementDefinition.xml
var assets embed.FS

type DWDDecoder struct {
	definitions map[string]MetElement
}

type MetElements struct {
	Definitions []MetElement `xml:"MetElement"`
}

type MetElement struct {
	Name        string `xml:"ShortName"`
	Unit        string `xml:"UnitOfMeasurement"`
	Description string `xml:"Description"`
}

type ProductDefinition struct {
	Issuer           string   `xml:"Issuer"`
	IssueTime        string   `xml:"IssueTime"`
	ReferencedModels []Model  `xml:"ReferencedModel>Model"`
	TimeSteps        []string `xml:"ForecastTimeSteps>TimeStep"`
}

type Model struct {
	Name    string `xml:"name,attr"`
	RefTime string `xml:"referenceTime,attr"`
}

type Placemark struct {
	Name        string     `xml:"name"`
	Description string     `xml:"description"`
	Point       string     `xml:"Point>coordinates"`
	Forecast    []Forecast `xml:"ExtendedData>Forecast"`
}

type Forecast struct {
	ElementName string `xml:"elementName,attr"`
	Value       string `xml:"value"`
}

type EnrichedPlacemark struct {
	Name        string
	Description string
	Latitude    float32
	Longitude   float32
	Forecast    map[string]EnrichedForecast
}

type EnrichedForecast struct {
	Name        string
	Description string
	Unit        string
	Values      map[int]float32
}

func newDWDDecoder() (DWDDecoder, error) {
	file, err := assets.Open(definitions)
	if err != nil {
		return DWDDecoder{}, err
	}
	defer func() { _ = file.Close() }()
	// Decode the definitions
	var elements MetElements
	decoder := xml.NewDecoder(file)
	// DWD xml is in ISO-8859-1
	decoder.CharsetReader = charset.NewReaderLabel
	err = decoder.Decode(&elements)
	if err != nil {
		return DWDDecoder{}, err
	}
	definitions := make(map[string]MetElement)
	for _, el := range elements.Definitions {
		definitions[el.Name] = el
	}
	return DWDDecoder{definitions}, nil
}

func (d DWDDecoder) run(filename string, ch chan<- interface{}) {
	err := d.runWithError(filename, ch)
	if err != nil {
		log.Fatal(err)
	}
}

func (d DWDDecoder) runWithError(filename string, ch chan<- interface{}) error {
	defer close(ch)
	archive, err := zip.OpenReader(filename)
	if err != nil {
		return fmt.Errorf("open %s: %s", filename, err)
	}
	defer func() { _ = archive.Close() }()
	archiveFile := archive.File[0]
	file, err := archiveFile.Open()
	if err != nil {
		return fmt.Errorf("open archive file %s: %s", archiveFile.Name, err)
	}
	decoder := xml.NewDecoder(file)
	// DWD xml is in ISO-8859-1
	decoder.CharsetReader = charset.NewReaderLabel
Decode:
	for {
		tok, err := decoder.Token()
		if err == io.EOF {
			break Decode
		} else if err != nil {
			return fmt.Errorf("decoding token: %s", err)
		}

		switch tokType := tok.(type) {
		case xml.StartElement:
			if tokType.Name.Local == "ProductDefinition" {
				var prod ProductDefinition
				err = decoder.DecodeElement(&prod, &tokType)
				if err != nil {
					return fmt.Errorf("decoding ProductDefinition: %s", err)
				}
				ch <- prod
			} else if tokType.Name.Local == "Placemark" {
				var place Placemark
				err = decoder.DecodeElement(&place, &tokType)
				if err != nil {
					return fmt.Errorf("decoding placemark: %s", err)
				}
				enriched, err := d.enrichPlacemark(place)
				if err != nil {
					return fmt.Errorf("enrich placemark: %s", err)
				}
				ch <- enriched
			}
		default:
			// Ignore
		}
	}
	return nil
}

func (d DWDDecoder) enrichPlacemark(place Placemark) (EnrichedPlacemark, error) {
	enriched := EnrichedPlacemark{
		Name:        place.Name,
		Description: place.Description,
	}
	lat, lon := normalizePoint(place.Point)
	enriched.Latitude = lat
	enriched.Longitude = lon
	enriched.Forecast = make(map[string]EnrichedForecast, len(place.Forecast))
	for _, f := range place.Forecast {
		var err error
		enforecast, err := d.enrichForecast(f)
		if err != nil {
			return enriched, err
		}
		enriched.Forecast[enforecast.Name] = enforecast
	}
	return enriched, nil
}

func normalizePoint(point string) (float32, float32) {
	lla := strings.Split(point, ",")
	lon, lat := lla[0], lla[1]
	latf, _ := strconv.ParseFloat(lat, 32)
	lonf, _ := strconv.ParseFloat(lon, 32)
	return float32(latf), float32(lonf)
}

func (d DWDDecoder) enrichForecast(forecast Forecast) (EnrichedForecast, error) {
	ret := EnrichedForecast{}
	def, ok := d.definitions[forecast.ElementName]
	if !ok {
		return ret, fmt.Errorf("no definition for %s", forecast.ElementName)
	}
	ret.Name = def.Name
	ret.Description = def.Description
	ret.Unit = def.Unit
	ret.Values = make(map[int]float32)
	values := strings.Fields(forecast.Value)
	for i, v := range values {
		if v == "-" {
			continue
		}
		f, err := strconv.ParseFloat(v, 32)
		if err != nil {
			return ret, fmt.Errorf("cannot parse float %s: %s", v, err)
		}
		ret.Values[i] = float32(f)
	}
	return ret, nil
}
