package main

import (
	"time"

	"github.com/nathan-osman/go-sunrise"
)

/*

ThunderyRain
LightRain
HeavyRain
Showers
Snow
Sleet
Fog
Clear
Storm
Cloudy
PartlyCloudy
Windy
*/

func describe(f *APIForecast, dewPoint float32) string {
	// Taken from mosmix_element_weather_xls.xls
	switch f.WeatherWarning {
	case 95:
		return "ModerateThunderstorm"
	case 86:
		return "SnowShower"
	case 85:
		return "LightSnowShower"
	case 84:
		return "MixedSnowRain"
	case 83:
		return "LightMixedSnowRain"
	case 82:
		return "HeavyRain"
	case 81:
		return "Rain"
	case 80:
		return "LightRain"
	case 75:
		return "HeavySnow"
	case 73:
		return "ModerateSnow"
	case 71:
		return "LightSnow"
	case 69:
		return "HeavySnowRain"
	case 68:
		return "LightSnowRain"
	case 67:
		return "FreezingRain"
	case 66:
		return "LightFreezingRain"
	case 65:
		return "HeavyRain"
	case 63:
		return "ModerateRain"
	case 61:
		return "LightRain"
	case 57:
		return "FreezingDrizzle"
	case 56:
		return "LightFreezingDrizzle"
	case 55:
		return "HeavyDrizzle"
	case 53:
		return "ModerateDrizzle"
	case 51:
		return "LightDrizzle"
	case 49:
		return "FrozenFog"
	case 45:
		return "Fog"
	case 03:
		return "CloudsForming"
	case 02:
		return "CloudsUnchanged"
	case 01:
		return "CloudsDissolving"
	case 00:
		return "NoCloudDevelopment"
	}
	// Heuristics
	switch {
	case f.Snow > 0.01 && f.Temp < zeroKelvin+10:
		return "Snow"
	case f.Rain > 0.5 && f.RainProb > 25.0:
		return "HeavyRain"
	case f.Rain > 0.2 && f.RainProb > 25.0:
		return "LightRain"
	case f.FogProb > 30.0:
		return "Fog"
	case f.WindSpeed > 9.0:
		return "Windy"
	case f.Clouds > 80:
		return "Cloudy"
	case f.Clouds > 25:
		return "PartlyCloudy"
	default:
		return "Clear"
	}
}

func sun(lat, lon float64, day time.Time) (time.Time, time.Time) {
	return sunrise.SunriseSunset(lat, lon,
		day.Year(), day.Month(), day.Day())
}
